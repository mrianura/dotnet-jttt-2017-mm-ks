﻿using System;
using System.Net;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MailKit.Net.Smtp;
using MailKit;
using MimeKit;

namespace hackaton
{
    class MailSender
    {
        private String login;
        private String password;
        private String host;
        private int port;

        public MailSender(String login, String password, String host, int port)
        {
            this.login = login;
            this.password = password;
            this.host = host;
            this.port = port;
        }

        public bool sendMail(String from, String to, String subject, String msg, string img)
        {
            var message = new MimeMessage();

            message.From.Add(new MailboxAddress(from));
            message.To.Add(new MailboxAddress(to, to));
            message.Subject = subject;

            var builder = new BodyBuilder();

            builder.TextBody = msg;
            using (var client = new WebClient())
            {
                client.DownloadFile(img, "test.png");
            }
            builder.Attachments.Add("test.png");
            message.Body = builder.ToMessageBody();

            using (var client = new SmtpClient())
            {
                client.ServerCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors) => true;

                try
                {
                    client.Connect(host, port);
                    client.AuthenticationMechanisms.Remove("XOAUTH2");
                    client.Authenticate(login, password);
                    client.Send(message);
                    client.Disconnect(true);
                    return true;
                }
                catch(Exception ex)
                {
                    throw ex;
                }
            }
        }
    }
}
