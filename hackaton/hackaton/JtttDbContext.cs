﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hackaton
{
    class JtttDbContext : DbContext
    {
        public JtttDbContext() : base("DajcieMiSieWyspacJttt_")
        {
            Configuration.LazyLoadingEnabled = true;
            Database.SetInitializer(new JtttDBInitialiser());
        }

        public DbSet<JTTTjob> JTTTjobs { get; set; }
        public DbSet<Task> Tasks { get; set; }
    }
}
