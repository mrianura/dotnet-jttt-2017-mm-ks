﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hackaton
{
    class Logger
    {

        public void addMessage(String msg)
        {
            using (StreamWriter handler = File.AppendText("log.txt"))
            {
                handler.Write("\r\nLog Entry : ");
                handler.WriteLine("{0} {1}", DateTime.Now.ToLongTimeString(), DateTime.Now.ToLongDateString());
                handler.WriteLine("  :");
                handler.WriteLine("  :{0}", msg);
                handler.WriteLine("-------------------------------");
            }
        }
    }
}
