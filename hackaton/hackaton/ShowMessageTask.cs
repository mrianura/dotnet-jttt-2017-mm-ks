﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Drawing;
using System.IO;
using System.Globalization;
using System.ComponentModel.DataAnnotations.Schema;

namespace hackaton
{
    [Table("ShowMessageTasks")]
    class ShowMessageTask : Task
    {
        static private UInt64 imgIdx = 0;

        public ShowMessageTask(){}

        public override void Run(Queue<Object> argQueue)
        {
            string msg = (string)argQueue.Dequeue();
            string source = (string)argQueue.Dequeue();
            DateTime localDate = DateTime.Now;
            string path = String.Format("test{0}.png", imgIdx.ToString());
            imgIdx++;
            using (var client = new WebClient())
            {
                client.DownloadFile(source, path);
            }

            Image img = Image.FromFile(path);
            MessageForm msgForm = new MessageForm(msg,img);
            msgForm.Show();

        }
    }
}
