﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hackaton
{
    public class JTTTjob
    {
        [Key, ForeignKey("Task")]
        public int JTTTjobId { get; set; }
        public Task Task { get; set; }

        public string name { get; set; }

        public JTTTjob() { }

        public JTTTjob(Task task, string name)
        {
            this.Task = (task);
            this.name = name;
            task.JTTTjob = this;
        }
        virtual public void Run() {}

    }
}
