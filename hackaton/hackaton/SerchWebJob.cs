﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using HtmlAgilityPack;
using System.Threading.Tasks;
using System.Net;
using System.ComponentModel.DataAnnotations.Schema;

namespace hackaton
{
    [Table("WebJobs")]
    class SerchWebJob : JTTTjob
    {
        public String url { get; set; }
        public String text { get; set; }


        public SerchWebJob() { }
        public SerchWebJob(Task task, String name,String url,String text) : base(task,name)
        {
            this.text = text;
            this.url = url;
        }

        private string GetPageHtml()
        {
            using (var wc = new WebClient())
            {
                wc.Encoding = Encoding.UTF8;
                var html = System.Net.WebUtility.HtmlDecode(wc.DownloadString(url));

                return html;
            }
        }

        public void SearchPageNodes()
        {
            var doc = new HtmlDocument();
            var pageHtml = GetPageHtml();
            //var mailSender = new MailSender("mrianura123@o2.pl", "Monitor123", "poczta.o2.pl", 465);

            doc.LoadHtml(pageHtml);

            var nodes = doc.DocumentNode.Descendants("img");

            foreach (var node in nodes)
            {

                var srcValue = node.GetAttributeValue("src", "");
                var altValue = node.GetAttributeValue("alt", "");

                if (altValue.ToLower().Contains(text.ToLower()))
                {
                    try
                    {
                        Queue<Object> argQueue = new Queue<Object>();
                        argQueue.Enqueue((Object)altValue);
                        argQueue.Enqueue((Object)srcValue);
                        Task.Run(argQueue);
                        
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }
            }
        }

        public override void Run()
        {
            if(!url.Substring(0, ("http://").Length).Contains("http://"))
            {
                url = "http://" + url;
            }
            SearchPageNodes();
        }
    }
}
