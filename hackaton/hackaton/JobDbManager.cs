﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace hackaton
{
    public class JobDbManager
    {
        public void addJob(JTTTjob a)
        {
            using (var ctx = new JtttDbContext())
            {
                ctx.JTTTjobs.Add(a);
                ctx.SaveChanges();
            }
        }

        public List<JTTTjob> getJobs()
        {
            List<JTTTjob> jobs = new List<JTTTjob>();

            using (var ctx = new JtttDbContext())
            {
                foreach (var job in ctx.JTTTjobs.Include("Task"))
                {
                    jobs.Add(job);
                }
            }
            return jobs;
        }

        public void clearAllJobs()
        {
            using (var ctx = new JtttDbContext())
            {
                ctx.JTTTjobs.RemoveRange(ctx.JTTTjobs);
                ctx.Tasks.RemoveRange(ctx.Tasks);
                ctx.SaveChanges();
            }
        }
    }
}
