﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using Newtonsoft.Json;
using System.Globalization;
using System.ComponentModel.DataAnnotations.Schema;

namespace hackaton
{
    enum Units { Default, Metric, Imperial};


    [Table("WeatherJobs")]
    class CheckTempJob : JTTTjob
    {
        public String city { get; set; }
        public String unit { get; set; }
        public decimal tempValue { get; set; }

        public CheckTempJob() { }

        public CheckTempJob(Task task,String name,String city, decimal tempValue,Units unit) : base(task,name)
        {
            this.city = CultureInfo.CurrentCulture.TextInfo.ToTitleCase(city.ToLower());
            this.tempValue = tempValue;
            this.unit = unit.ToString();
        }

        public string GetPageHtml()
        {
            using (var wc = new WebClient())
            {
                wc.Encoding = Encoding.UTF8;
                var html = System.Net.WebUtility.HtmlDecode(wc.DownloadString(
                    String.Format("http://api.openweathermap.org/data/2.5/weather?q={0}?&units={1}&APPID=19f4e9698337ce4151d4e3407ec1fd4a",
                    city, unit)));
                return html;
            }
        }

        public override void Run()
        {
            String json = GetPageHtml();
            if(json == "{\"cod\":\"404\",\"message\":\"city not found\"}")
            {
                throw new System.ArgumentException("city not found");
            }
            WeatherObject currentWeather = JsonConvert.DeserializeObject<WeatherObject>(json);

       
            if (Convert.ToDecimal(currentWeather.main.temp) > tempValue)
            {
                String message = String.Format("Current weather:\nin {0} is {1}: \naverage temperature: {2:0.00}, pressure: {3:0.00}, humidity: {4:0.00}, max temperaute: {5:0.00}, min temperature {6:0.00}, wind speed {7:0.00}, wind angle {8:0.00}\n cloudines: {9}%\n sunrise {10}, sunset{11} ",
                    city,
                    currentWeather.weather[0].description,
                    currentWeather.main.temp,
                    currentWeather.main.pressure,
                    currentWeather.main.humidity,
                    currentWeather.main.temp_max,
                    currentWeather.main.temp_min,
                    currentWeather.wind.speed,
                    currentWeather.wind.deg,
                    currentWeather.clouds.all,
                    TimeSpan.FromSeconds(currentWeather.sys.sunrise).ToString(@"hh\:mm\:ss"),
                    TimeSpan.FromSeconds(currentWeather.sys.sunset).ToString(@"hh\:mm\:ss")
                    );
                Queue<Object> argQueue = new Queue<Object>();
                argQueue.Enqueue((Object)message);
                argQueue.Enqueue((Object)String.Format("http://openweathermap.org/img/w/{0}.png", currentWeather.weather[0].icon));

                Task.Run(argQueue);
            }
        }
    }
}
