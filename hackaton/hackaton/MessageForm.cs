﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;

namespace hackaton
{
    public partial class MessageForm : Form
    {
        private Image img;
        public MessageForm(String msg,Image img)
        {
            InitializeComponent();
            textBox.Text = msg;
            this.img = img;
            pictureBox.Image = img;
        }

        private void Message_Load(object sender, EventArgs e)
        {

        }
    }
}
