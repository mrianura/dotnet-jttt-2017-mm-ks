﻿namespace hackaton
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ActiveTasksBox = new System.Windows.Forms.ListView();
            this.textBox9 = new System.Windows.Forms.TextBox();
            this.StartButton = new System.Windows.Forms.Button();
            this.NameBox = new System.Windows.Forms.TextBox();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.ClearButton = new System.Windows.Forms.Button();
            this.cityBox = new System.Windows.Forms.TextBox();
            this.tempValueBox = new System.Windows.Forms.NumericUpDown();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.textBox = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.URLbox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.conditionTab = new System.Windows.Forms.TabControl();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.label5 = new System.Windows.Forms.Label();
            this.taskTab = new System.Windows.Forms.TabControl();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.EmailBox = new System.Windows.Forms.TextBox();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.tempValueBox)).BeginInit();
            this.tabPage5.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.conditionTab.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.taskTab.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.SuspendLayout();
            // 
            // ActiveTasksBox
            // 
            this.ActiveTasksBox.Location = new System.Drawing.Point(451, 38);
            this.ActiveTasksBox.Name = "ActiveTasksBox";
            this.ActiveTasksBox.Size = new System.Drawing.Size(308, 228);
            this.ActiveTasksBox.TabIndex = 12;
            this.ActiveTasksBox.UseCompatibleStateImageBehavior = false;
            this.ActiveTasksBox.View = System.Windows.Forms.View.List;
            // 
            // textBox9
            // 
            this.textBox9.BackColor = System.Drawing.SystemColors.Control;
            this.textBox9.Location = new System.Drawing.Point(451, 12);
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new System.Drawing.Size(62, 20);
            this.textBox9.TabIndex = 13;
            this.textBox9.Text = "Actual List:";
            // 
            // StartButton
            // 
            this.StartButton.Location = new System.Drawing.Point(451, 282);
            this.StartButton.Name = "StartButton";
            this.StartButton.Size = new System.Drawing.Size(146, 44);
            this.StartButton.TabIndex = 14;
            this.StartButton.Text = "Start";
            this.StartButton.UseVisualStyleBackColor = true;
            this.StartButton.Click += new System.EventHandler(this.startButton_Click);
            // 
            // NameBox
            // 
            this.NameBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.NameBox.Location = new System.Drawing.Point(104, 268);
            this.NameBox.Name = "NameBox";
            this.NameBox.Size = new System.Drawing.Size(313, 20);
            this.NameBox.TabIndex = 15;
            // 
            // textBox5
            // 
            this.textBox5.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.textBox5.Location = new System.Drawing.Point(45, 268);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(55, 20);
            this.textBox5.TabIndex = 16;
            this.textBox5.Text = "Name:";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(37, 310);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(96, 31);
            this.button1.TabIndex = 17;
            this.button1.Text = "Add";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.Add_Click);
            // 
            // ClearButton
            // 
            this.ClearButton.Location = new System.Drawing.Point(619, 282);
            this.ClearButton.Name = "ClearButton";
            this.ClearButton.Size = new System.Drawing.Size(140, 44);
            this.ClearButton.TabIndex = 18;
            this.ClearButton.Text = "Clear";
            this.ClearButton.UseVisualStyleBackColor = true;
            this.ClearButton.Click += new System.EventHandler(this.ClearButton_Click);
            // 
            // cityBox
            // 
            this.cityBox.Location = new System.Drawing.Point(84, 33);
            this.cityBox.Name = "cityBox";
            this.cityBox.Size = new System.Drawing.Size(296, 20);
            this.cityBox.TabIndex = 3;
            // 
            // tempValueBox
            // 
            this.tempValueBox.Location = new System.Drawing.Point(84, 59);
            this.tempValueBox.Maximum = new decimal(new int[] {
            50,
            0,
            0,
            0});
            this.tempValueBox.Minimum = new decimal(new int[] {
            80,
            0,
            0,
            -2147483648});
            this.tempValueBox.Name = "tempValueBox";
            this.tempValueBox.Size = new System.Drawing.Size(120, 20);
            this.tempValueBox.TabIndex = 22;
            // 
            // tabPage5
            // 
            this.tabPage5.BackColor = System.Drawing.Color.White;
            this.tabPage5.Controls.Add(this.tempValueBox);
            this.tabPage5.Controls.Add(this.cityBox);
            this.tabPage5.Controls.Add(this.textBox2);
            this.tabPage5.Controls.Add(this.textBox8);
            this.tabPage5.Controls.Add(this.label2);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(389, 91);
            this.tabPage5.TabIndex = 2;
            this.tabPage5.Text = "Check Tempeature";
            // 
            // textBox2
            // 
            this.textBox2.BackColor = System.Drawing.SystemColors.Window;
            this.textBox2.Location = new System.Drawing.Point(9, 59);
            this.textBox2.Name = "textBox2";
            this.textBox2.ReadOnly = true;
            this.textBox2.Size = new System.Drawing.Size(69, 20);
            this.textBox2.TabIndex = 4;
            this.textBox2.Text = "Temperature:";
            this.textBox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox8
            // 
            this.textBox8.BackColor = System.Drawing.SystemColors.Window;
            this.textBox8.Location = new System.Drawing.Point(9, 33);
            this.textBox8.Name = "textBox8";
            this.textBox8.ReadOnly = true;
            this.textBox8.Size = new System.Drawing.Size(69, 20);
            this.textBox8.TabIndex = 6;
            this.textBox8.Text = "City:";
            this.textBox8.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(8, 17);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(280, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Check if actual temperature in city is grather than selecetd";
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.White;
            this.tabPage1.Controls.Add(this.textBox4);
            this.tabPage1.Controls.Add(this.textBox);
            this.tabPage1.Controls.Add(this.textBox1);
            this.tabPage1.Controls.Add(this.URLbox);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(389, 91);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Search WWW";
            // 
            // textBox4
            // 
            this.textBox4.BackColor = System.Drawing.SystemColors.Window;
            this.textBox4.Location = new System.Drawing.Point(9, 59);
            this.textBox4.Name = "textBox4";
            this.textBox4.ReadOnly = true;
            this.textBox4.Size = new System.Drawing.Size(55, 20);
            this.textBox4.TabIndex = 4;
            this.textBox4.Text = "Text:";
            this.textBox4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBox
            // 
            this.textBox.Location = new System.Drawing.Point(68, 59);
            this.textBox.Name = "textBox";
            this.textBox.Size = new System.Drawing.Size(313, 20);
            this.textBox.TabIndex = 5;
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.SystemColors.Window;
            this.textBox1.Location = new System.Drawing.Point(9, 33);
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(55, 20);
            this.textBox1.TabIndex = 6;
            this.textBox1.Text = "WWW:";
            this.textBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // URLbox
            // 
            this.URLbox.Location = new System.Drawing.Point(68, 33);
            this.URLbox.Name = "URLbox";
            this.URLbox.Size = new System.Drawing.Size(313, 20);
            this.URLbox.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(316, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "Search for image in WWW page with description containing Text.";
            // 
            // conditionTab
            // 
            this.conditionTab.Controls.Add(this.tabPage1);
            this.conditionTab.Controls.Add(this.tabPage5);
            this.conditionTab.Location = new System.Drawing.Point(32, 38);
            this.conditionTab.Name = "conditionTab";
            this.conditionTab.SelectedIndex = 0;
            this.conditionTab.Size = new System.Drawing.Size(397, 117);
            this.conditionTab.TabIndex = 21;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.label5);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(389, 56);
            this.tabPage2.TabIndex = 2;
            this.tabPage2.Text = "Show message";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 14);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(147, 13);
            this.label5.TabIndex = 8;
            this.label5.Text = "Display results in new window";
            // 
            // taskTab
            // 
            this.taskTab.Controls.Add(this.tabPage3);
            this.taskTab.Controls.Add(this.tabPage2);
            this.taskTab.Location = new System.Drawing.Point(32, 184);
            this.taskTab.Name = "taskTab";
            this.taskTab.SelectedIndex = 0;
            this.taskTab.Size = new System.Drawing.Size(397, 82);
            this.taskTab.TabIndex = 22;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.EmailBox);
            this.tabPage3.Controls.Add(this.textBox7);
            this.tabPage3.Controls.Add(this.label3);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage3.Size = new System.Drawing.Size(389, 56);
            this.tabPage3.TabIndex = 0;
            this.tabPage3.Text = "Send Mail";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // EmailBox
            // 
            this.EmailBox.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.EmailBox.Location = new System.Drawing.Point(68, 30);
            this.EmailBox.Name = "EmailBox";
            this.EmailBox.Size = new System.Drawing.Size(313, 20);
            this.EmailBox.TabIndex = 10;
            // 
            // textBox7
            // 
            this.textBox7.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.textBox7.Location = new System.Drawing.Point(9, 30);
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new System.Drawing.Size(55, 20);
            this.textBox7.TabIndex = 9;
            this.textBox7.Text = "Email:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 14);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(171, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Send results to given emal address";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(789, 357);
            this.Controls.Add(this.taskTab);
            this.Controls.Add(this.conditionTab);
            this.Controls.Add(this.NameBox);
            this.Controls.Add(this.textBox5);
            this.Controls.Add(this.ClearButton);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.StartButton);
            this.Controls.Add(this.textBox9);
            this.Controls.Add(this.ActiveTasksBox);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.tempValueBox)).EndInit();
            this.tabPage5.ResumeLayout(false);
            this.tabPage5.PerformLayout();
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.conditionTab.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.taskTab.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ListView ActiveTasksBox;
        private System.Windows.Forms.TextBox textBox9;
        private System.Windows.Forms.Button StartButton;
        private System.Windows.Forms.TextBox NameBox;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button ClearButton;
        private System.Windows.Forms.TextBox cityBox;
        private System.Windows.Forms.NumericUpDown tempValueBox;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.TextBox textBox;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox URLbox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabControl conditionTab;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TabControl taskTab;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TextBox EmailBox;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.Label label3;
    }
}

