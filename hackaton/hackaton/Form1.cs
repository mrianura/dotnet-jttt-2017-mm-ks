﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace hackaton
{
    public partial class Form1 : Form
    {

        public JobDbManager jobDbManager { get; set; } = new JobDbManager();
        private List<JTTTjob> JTTTjobs  = new List<JTTTjob>();

        private Logger logger = new Logger();

        private const int SendMailId = 0;
        private const int ShowMessageId = 1;

        private const int SearchWebId = 0;
        private const int CheckTempId = 1;

        public Form1()
        {
            InitializeComponent();
            foreach (JTTTjob a in jobDbManager.getJobs())
            {
                JTTTjobs.Add(a);
                ActiveTasksBox.Items.Add(a.name);
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
        }

        private void startButton_Click(object sender, EventArgs e)
        {
            int doneTasks = 0;
            int errors = 0;
            foreach (var job in JTTTjobs)
            {
                try
                {
                   job.Run();
                   doneTasks++;
                }
                catch(Exception ex)
                {
                    errors++;
                    String errorMsg = "FATAL ERROR: Task " + job.name + " failed ";
                    MessageBox.Show(
                    ex.Message,
                    errorMsg,
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Error
                    );
                    logger.addMessage(errorMsg + "\n" + ex.Message);
                }
            }

            String returnMsg = "Done tasks: " + doneTasks.ToString();
            if (errors > 0)
            {
                returnMsg += ", " + errors.ToString() + " tasks failed";
            }

            logger.addMessage(returnMsg);

            MessageBox.Show(
            returnMsg,
            "Status",
            MessageBoxButtons.OK,
            MessageBoxIcon.Warning
            );
            return;
        }

        private void EmptyFieldError(String fieldName)
        {
            MessageBox.Show(
                fieldName + " field can not be empty",
                "ERROR",
                MessageBoxButtons.OK,
                MessageBoxIcon.Warning
            );
        }

        private void Add_Click(object sender, EventArgs e)
        {
            var name = NameBox.Text;

            JTTTjob job = null;
            Task task = null;

            if (name == "")
            {
                EmptyFieldError("name");
                return;
            }


            switch (taskTab.SelectedIndex)
            {

                case SendMailId:
                    var email = EmailBox.Text;
                    if (email == "")
                    {
                        EmptyFieldError("email");
                        return;
                    }
                    task = new SendMailTask(email);
                    break;
                case ShowMessageId:
                    task = new ShowMessageTask();
                    break;
            }

            switch (conditionTab.SelectedIndex)
            {
                case SearchWebId:
                    var url = URLbox.Text;
                    var text = textBox.Text;

                    if (url == "")
                    {
                        EmptyFieldError("url");
                        return;
                    }

                    if (text == "")
                    {
                        EmptyFieldError("text");
                        return;
                    }

                    job = new SerchWebJob(task,name,url,text);
                    break;
                case CheckTempId:
                    var city = cityBox.Text;
                    var tempValue = tempValueBox.Value;

                    if (city == "")
                    {
                        EmptyFieldError("city");
                        return;
                    }

                    job = new CheckTempJob(task,name,city,tempValue,Units.Metric);
                    break;
            }
            JTTTjobs.Add(job);
            ActiveTasksBox.Items.Add(name);
            jobDbManager.addJob(job);
        }

        private void ClearButton_Click(object sender, EventArgs e)
        {
            JTTTjobs.Clear();
            ActiveTasksBox.Items.Clear();
            jobDbManager.clearAllJobs();
        }

    }
}